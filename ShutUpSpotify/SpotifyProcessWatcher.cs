﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Serilog;

namespace ShutUpSpotify;

public sealed class SpotifyProcessWatcher
{
    private const int ProcessPollingIntervalMilliseconds = 500;
    private const int WaitTimeMillisecondsAfterNoSpotifyProcessFoundMs = 5000;

    private static readonly IReadOnlyCollection<string> KeywordsToSearch = new[]
    {
        "Advertisement",
        "Spotify"
    };

    private bool _isAdvertisementRunning;
    private bool _isProcessWatcherRunning;

    public void Start()
    {
        _isProcessWatcherRunning = true;
        while (_isProcessWatcherRunning)
        {
            RunCycle();
            Thread.Sleep(ProcessPollingIntervalMilliseconds);
        }
    }

    private void RunCycle()
    {
        Process[] spotifyProcesses = GetSpotifyProcesses();
        if (!spotifyProcesses.Any() || !_isProcessWatcherRunning)
        {
            Log.Information("No Spotify process found, waiting {Delay} ms...", WaitTimeMillisecondsAfterNoSpotifyProcessFoundMs);
            Thread.Sleep(WaitTimeMillisecondsAfterNoSpotifyProcessFoundMs);
            return;
        }

        MuteIfAdJustStarted(spotifyProcesses);
        UnmuteIfAdJustFinished(spotifyProcesses);
    }

    private static bool IsAdvertisementActiveNow(Process[] spotifyProcesses)
    {
        return spotifyProcesses.Any(
            process => KeywordsToSearch.Contains(process.MainWindowTitle, StringComparer.OrdinalIgnoreCase));
    }

    private void MuteIfAdJustStarted(Process[] spotifyProcesses)
    {
        bool isAdNow = IsAdvertisementActiveNow(spotifyProcesses);
        bool adStarted = !_isAdvertisementRunning && isAdNow;
        if (adStarted)
        {
            _isAdvertisementRunning = true;
            Log.Information("Ad started, muting");
            ProcessMuter.Mute(spotifyProcesses);
        }
    }

    private void UnmuteIfAdJustFinished(Process[] spotifyProcesses)
    {
        bool isAdNow = IsAdvertisementActiveNow(spotifyProcesses);
        bool adFinished = _isAdvertisementRunning && !isAdNow;
        if (adFinished)
        {
            _isAdvertisementRunning = false;
            Log.Information("Ad ended, unmuting");
            ProcessMuter.Unmute(spotifyProcesses);
        }
    }

    public void Stop()
    {
        _isProcessWatcherRunning = false;
        ProcessMuter.Unmute(GetSpotifyProcesses());
    }

    private static Process[] GetSpotifyProcesses() =>
        Process.GetProcessesByName("Spotify");
}