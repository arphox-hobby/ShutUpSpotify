﻿using System;
using System.Runtime.InteropServices;
using Serilog;

namespace ShutUpSpotify;

public static class Program
{
    private static readonly SpotifyProcessWatcher Service = new();

    public static void Main()
    {
        SetupLogging();
        ShutdownIfNotOnWindows();
        Log.Information("Started. Press Ctrl+C to exit gracefully");
        SubscribeCancelKeyPress();
        try
        {
            Service.Start();
        }
        finally
        {
            Service.Stop();
        }
    }

    private static void ShutdownIfNotOnWindows()
    {
        if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
        {
            Log.Error("This program is only supported on Windows. Press any key to exit");
            Console.ReadKey();
            Environment.Exit(1);
        }
    }

    private static void SetupLogging()
    {
        Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Verbose()
            .WriteTo.Console(
                outputTemplate: "[{Timestamp:HH:mm:ss.ffffff} {Level:u3}] {Message:lj}{NewLine}{Exception}")
            .CreateLogger();
        
        // Log.Verbose("Verbose example with value {Value}", 42);
        // Log.Debug("Debug example with value {Value}", 42);
        // Log.Information("Information example with value {Value}", 42);
        // Log.Warning("Warning example with value {Value}", 42);
        // Log.Error("Error example with value {Value}", 42);
        // Log.Fatal("Fatal example with value {Value}", 42);
    }

    private static void SubscribeCancelKeyPress()
    {
        Console.CancelKeyPress += (_, _) =>
        {
            Log.Information("Cancel request received. Exiting gracefully...");
            Service.Stop();
        };
    }
}